
$(document).ready(function() {
	let count = 0;
	let maxCount = 10;

	function start() {
		if(count < maxCount) {
			count++
			fetch('https://secure-castle-8699.herokuapp.com/')
				.then((res) =>  res.json())
				.then((data) => {
					const color = ['purple', 'green', 'blue', 'orange', 'red']
					const colorRand = color[Math.floor(Math.random() * color.length)];
					const style = { height: Math.floor(data.num) * 10 };
					const div = $('<div class="test"></div>')
						.css(style)
						.css('background-color', colorRand);
					$('.container').append(div);
					console.log(`Height: ${style.height} Color:${colorRand}`);
			});
			setTimeout(start, 1000);
		};
	};
	start();
});

